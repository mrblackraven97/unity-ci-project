if (-not (Test-Path $args[0] -PathType Leaf)) 
{
    throw 'The file does not exist : probably some of the test failed'
}
[xml]$xmldata = get-content $args[0]
"Result: of "+$xmldata.'test-run'.total+" tests, "+$xmldata.'test-run'.passed+" passed and "+$xmldata.'test-run'.failed+" failed. `n "

$xmlnav = $xmldata.'test-run'

if($xmlnav.GetAttribute("testcasecount") -gt 0)
{

    while($xmlnav.GetAttribute("type") -ne "TestFixture")
    {
        $xmlnav = $xmlnav.'test-suite'
    }

    $results = New-Object System.Collections.ArrayList

    Foreach ($case in $xmlnav.'test-case')
    {
        $results.Add([pscustomobject]@{ClassName = $case.classname; Test = $case.name; Result = $case.result}) | Out-Null
    }

    "Test cases result:"
    $results | ft -GroupBy ClassName -Property Test, Result
}