using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using System;
using Microsoft.MixedReality.Toolkit.Build.Editor;

public class BuildApp : MonoBehaviour
{
   [MenuItem("Build/Build UWP")]
   public static void Build()
   {
      Debug.Log("*********** START BUILD ************");
      
      UwpBuildInfo ubi = new UwpBuildInfo();
      ubi.OutputDirectory = "App";
      ubi.BuildPlatform = "x64";
      ubi.BuildAppx = true;
      
      BuildReport report = UnityPlayerBuildTools.BuildUnityPlayer(ubi);

      Debug.Log("*********** BUILD "+report.summary.result+" ************");
      int returnCode = (int)report.summary.result - 1;
      Debug.Log("*********** EXITING WITH CODE "+ returnCode +" ************");

      if(UnityEditorInternal.InternalEditorUtility.inBatchMode)
      {
         EditorApplication.Exit(returnCode);
      }
   }
}