## Continuous delivery the Gitflow way

Instead of a single master branch, this approach uses two branches to track the history of the project. 

- **master** branch contains tags and/or commits that record the project's official release history
- **develop** is a shared integration branch which gives your team a place to ferret out bugs and incompatible changes.

For each feature one creates it's own branch to develop and test individually: once finished and its build **has passed all unit tests** (is "green") it is integrated in the develop branch.

It's important that the feature also passes all the integration tests in the develop branch as well: it's imperative that the develop branch remains "green", which means that all tests are passed, since every other developer bases its own work on this branch as mainline. This is why if the integration is unsuccessful it must be resolved as soon as possible.

![cicd-gitflow](cicd-gitflow.png) 

<div style="page-break-after: always;"></div>
## CI and GitFlow but on Unity

From resource:

https://engineering.etermax.com/continuous-integration-in-unity-with-gitlab-ci-cd-part-1-e902c94c0847

Test repo:

https://gitlab.com/mrblackraven97/unity-ci-project

TLDR:

1. Create the tests in a unity C# script in the assets folder
2. Create the .yml file containing the pipeline of tests; all these tests will be performed on a machine that has Unity installed: for this reason, I registered a runner on my local machine (since it has unity) which will execute the jobs for the CI integration tests on gitlab remotely
   1. It will contain a job for the unit-tests, a job for the build automation and a job for the deployment automation
   2. Each one of them has a script that is executed: the result of it determines the success or failure of the job
   3. Some helper scripts can be invoked before or after the main script in order to print information, setup environment etc...
3. Setup the runner on CI/CD on GitLab by naming the file `.gitlab-ci.yml`
4. Commit on the repo and see the test going in the CI/CD pipeline section on the website

Example Unity Test Suite Script

```c#
using NUnit.Framework;

public class NewTestSuite 
{
    [Test]
    public void SomeGreenTestPassing()
    {
        Assert.Pass();
    }

    [Test]
    public void SomeRedTestNotPassing()
    {
        Assert.Fail();
    }
}
```

Example .yml

``` yaml
stages:
 - test
 - build
 - deploy

# first stage: test 
# its script invokes unity cli and executes the tests for the project in this repo 
# if the tests pass or fail, the after script is executed in order to print the details of the test
# results. TODO parse the xml to make it more readable
unit-test:
 stage: test
 script: 
  - '& "C:\Program Files\Unity2019\Editor\Unity.exe" `
    -batchmode `
    -projectPath . `
    -runTests -testPlatform editmode `
    -logFile - `
    -testResults .\unit-tests-result.xml | Out-Default'
 after_script:
    - cat .\unit-tests-result.xml
 tags: 
  - unity

# second stage: build
# for now it's only an echo, but the script should be for the build of the project
unity-build:
 stage: build
 script: '& "C:\Program Files\Unity2019\Editor\Unity.exe" `
  -batchmode `
  -projectPath . `
  -executemethod BuildApp.Build'
 tags:
  - unity

# third stage: deployment
# for now it's only an echo, but the script should be for the deployment of the project
playstore:
 stage: deploy
 script: echo 'Deploying...'
 tags:
  - unity
```

TODO: try to setup the runner on a VM server on azure with unity 2019 installed; this way anyone can contribute and check if the pipeline of tests is correct